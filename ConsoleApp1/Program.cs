﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Xml;
using System.Xml.Linq;


namespace ConsoleApp1
{
    class Program
    {
        #region variables
        private static List<Newtonsoft.Json.Linq.JToken> competitions;
        static private ValamiNS.Plays team;
        static Dictionary<int, ValamiNS.Plays> dict = new Dictionary<int, ValamiNS.Plays>();
        public static List<int> mIDList = new List<int>();
        private static Timer oddsTimer = new Timer();
        private static Timer playCheck = new Timer();
        private static Timer instince = new Timer();
        private struct Oddds {
            public string match;
            public string team1;
            public double team1odds;
            public double drawodds;
            public string team2;
            public double team2odds;
        }
        #endregion
        #region methods
        static void Main(string[] args)
        {
            oddsTimer.Elapsed += OddsTimer_Elapsed;
            oddsTimer.Enabled = true;
            oddsTimer.Stop();
            OddsTimer_Elapsed(null, null);

            playCheck.Interval = (new TimeSpan(0, 10, 0).TotalMilliseconds);
            playCheck.Elapsed += PlayCheck_Elapsed;
            playCheck.Enabled = true;

            instince.Elapsed += Instince_Elapsed;
            instince.Enabled = true;
            instince.Stop();

            Console.ReadKey();
        }
        static private string JsonValue(string prop, object obj)
        {
            var result = string.Empty;
            var array = prop.Split('.');
            for (int i = 0; i < array.Length - 1; i++)
            {
                if (((Newtonsoft.Json.Linq.JObject)obj).GetValue(array[i]) != null)
                {
                    obj = ((Newtonsoft.Json.Linq.JObject)obj).GetValue(array[i]);
                }
            }
            if (((Newtonsoft.Json.Linq.JObject)obj).GetValue(array[array.Length - 1]) != null)
            {
                result = ((Newtonsoft.Json.Linq.JObject)obj).GetValue(array[array.Length - 1]).ToString();
            }
            return result;
        }
        #endregion
        #region events
        private static void Instince_Elapsed(object sender, ElapsedEventArgs e)
        {

            instince.Stop();
            team = new ValamiNS.Plays { mID = mIDList[0]};
            team.GoalArrived += Team_GoalArrived;
            team.Init();
            dict.Add(mIDList[0], team);
            if (JsonValue("date", competitions[0]) == JsonValue("date", competitions[1]))
            {
                team = new ValamiNS.Plays { mID = mIDList[1] };
                team.GoalArrived += Team_GoalArrived;
                team.Init();
                dict.Add(mIDList[1], team);
            }
        }
        private static void PlayCheck_Elapsed(object sender, ElapsedEventArgs e)
        {
            playCheck.Stop();
            try
            {
                string address = "http://api.football-data.org/v1/fixtures/";
                WebClient _client = new WebClient();
                _client.Headers.Add("X-Auth-Token", System.Configuration.ConfigurationManager.AppSettings["footballdataapikey"]);
                _client.Headers.Add("X-Response-Control", "minified");
                _client.Headers.Add("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/2.0.0.9");
                string resultJson = _client.DownloadString(address);
                var jobj = JsonConvert.DeserializeObject(resultJson);
                competitions = ((Newtonsoft.Json.Linq.JObject)jobj).Children().Last().First.ToList();

                foreach (var competition in competitions)
                {
                    if (((Convert.ToDateTime(((Newtonsoft.Json.Linq.JObject)competition).Children().ElementAt(2).First.ToString())) + new TimeSpan(2, 0, 0) - DateTime.Now).TotalMilliseconds > new TimeSpan(0, 30, 0).TotalMilliseconds)
                    {
                        mIDList.Add(Convert.ToInt32(((Newtonsoft.Json.Linq.JObject)competition).Children().ElementAt(0).First.ToString()));

                    }
                    else
                    {
                        break;
                    }
                }
                if (mIDList.Count != 0)
                {

                    instince.Interval = (Convert.ToDateTime(JsonValue("date", competitions[0])).TimeOfDay + new TimeSpan(2, 0, 0) - System.DateTime.Now.TimeOfDay).TotalMilliseconds;
                    instince.Start();
                    playCheck.Enabled = false;
                }
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error: " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
            }
            playCheck.Start();

        }
        private static void OddsTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var timeNow = System.DateTime.Now.TimeOfDay;
            var xmlCheckTime = new TimeSpan(24, 0, 0);
            var timecheck = xmlCheckTime - timeNow;
            oddsTimer.Interval = timecheck.TotalMilliseconds;
            var xDocument =  XElement.Load("http://xml.cdn.betclic.com/odds_en.xml");
            IEnumerable<XElement> sport = from el in xDocument.Elements("sport") where (string)el.Attribute("name") == "Football" select el;
            IEnumerable<XElement> cup = from el in sport.Elements("event") where (string)el.Attribute("name") == "World Cup" select el;
            IEnumerable<XElement> match = from el in cup.Elements("match") where (string)el.Attribute("live_id") != "" select el; 
            IEnumerable < XElement > odds = from el in match.Elements("bets").Elements("bet") where (string)el.Attribute("name") == "Match Result" select el ;
            Oddds smth = new Oddds();
            List<Oddds> olist = new List<Oddds>();
           
            foreach (XElement ele in odds)
            {
                var team1odds = (from el in ele.Elements("choice") where (string)el.Attribute("name") == "%1%" select el).First().ToString();
                var team2odds = (from el in ele.Elements("choice") where (string)el.Attribute("name") == "%2%" select el).First().ToString();
                var drawodds = (from el in ele.Elements("choice") where (string)el.Attribute("name") == "Draw" select el).First().ToString();
                smth.team1odds = Convert.ToDouble(XDocument.Parse(team1odds).Root.Attribute("odd").Value.Replace('.', ','));
                smth.team2odds = Convert.ToDouble(XDocument.Parse(team2odds).Root.Attribute("odd").Value.Replace('.', ','));
                smth.drawodds = Convert.ToDouble(XDocument.Parse(drawodds).Root.Attribute("odd").Value.Replace('.', ','));
                
                olist.Add(smth);
                
            }
            int p = 0;
            var aa = olist.ToArray();
            foreach (var ele in match)
            {
                smth.match = XDocument.Parse(ele.ToString()).Root.Attribute("name").Value;
                smth.team1 = XDocument.Parse(ele.ToString()).Root.Attribute("name").Value.Split('-')[0].TrimEnd(' ');
                smth.team2 = XDocument.Parse(ele.ToString()).Root.Attribute("name").Value.Split('-')[1].TrimStart(' ');

                aa[p].match = smth.match;
                aa[p].team1 = smth.team1;
                aa[p].team2 = smth.team2;
                
                p++;
            }
            olist = aa.ToList();
            foreach (var item in olist)
            {
                Console.WriteLine($"{item.match}: {item.team1} odds: {item.team1odds} - Döntetlen odds: {item.drawodds} - {item.team2} odds: {item.team2odds}" );
            }
            xDocument = null;
            aa = null;
            sport = null;
            cup = null;
            match = null;
            odds = null;
       
            oddsTimer.Start();
        }
        private static void Team_GoalArrived(object sender, ValamiNS.Plays.DataGoalEventArgs e)
        {
            switch (e.State)
            {
                case 0:
                    Console.WriteLine("Várakozás a focistákra...");
                    break;
                case 1:
                    if (team.isIn_Play == false)
                    {
                        Console.WriteLine(System.DateTime.Now + " " + e.Message);
                        team.isIn_Play = true;
                    }
                    else
                    {
                        Console.WriteLine(System.DateTime.Now + " " + e.Message + " Az állás most: " + e.Team1 + " " + e.goalsHomeTeam + " - " + e.goalsAwayTeam + " " + e.Team2);
                    }
                    break;
                case 2:
                    Console.WriteLine(System.DateTime.Now + " " + e.Message + " A végső állás: " + e.Team1 + " " + e.goalsHomeTeam + " - " + e.goalsAwayTeam + " " + e.Team2);
                    playCheck.Enabled = true;
                    playCheck.Start();
                    dict.Remove(team.mID);
                    break;
                case 3:
                    Console.WriteLine(System.DateTime.Now + " " + e.Message + " A végső állás: " + e.Team1 + " " + e.goalsHomeTeam + " - " + e.goalsAwayTeam + " " + e.Team2);
                    playCheck.Enabled = true;
                    playCheck.Start();
                    dict.Remove(team.mID);
                    break;
                default:
                    break;
            }
        }
        #endregion
    }
}
