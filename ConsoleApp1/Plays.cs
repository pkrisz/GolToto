﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Xml;
using System.Xml.Linq;

namespace ValamiNS
{   
    class Plays
    {
        #region variables
        private Timer mainTimer = new Timer();
        private Timer matchWatchdogTimer = new Timer();
       
        public String address;
        public int mID { get; set; }
        public WebClient _client = new WebClient();
        private static int GoalTeamHome = 0;
        private static int GoalTeamAway = 0;
        public bool isIn_Play = false;
        private int team1goals = GoalTeamHome;
        private int team2goals = GoalTeamAway;
        #endregion
        #region event_declaration
        public event EventHandler<DataGoalEventArgs> GoalArrived;
        public class DataGoalEventArgs : EventArgs, IDataGoalEventArgs
        {
            public int State { get; set; }
            public string Team1 { get; set; }
            public string Team2 { get; set; }
            public string Message { get; set; }
            public int goalsHomeTeam { get; set; }
            public int goalsAwayTeam { get; set; }
        }
        public interface IDataGoalEventArgs
        {
            int State { get; set; }
            string Team1 { get; set; }
            string Team2 { get; set; }
            string Message { get; set; }
            int goalsHomeTeam { get; set; }
            int goalsAwayTeam { get; set; }
        }
        #endregion
        #region events
        private void MachWatchdogTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            GoalArrived?.Invoke(this, new DataGoalEventArgs() { Message = "Időtúllépés miatt a meccs befejezettnek tekintett", State = 3 });
            DisposeClass();
        }
        public void InPlayCheck(Object source, System.Timers.ElapsedEventArgs e)
        {
            mainTimer.Stop();
            if (Probe())
            {
                DisposeClass();
            }
            else
            {
                mainTimer.Start();
            }
        }        
        #endregion
        #region methods
        public Plays()
        {
        }
        public void Init()
        {
            try
            {
                address = $"http://api.football-data.org/v1/fixtures/{mID}?head2head=0";

                _client.Headers.Add("X-Auth-Token", System.Configuration.ConfigurationManager.AppSettings["footballdataapikey"]);
                _client.Headers.Add("X-Response-Control", "minified");
                _client.Headers.Add("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/2.0.0.9");
                
                #region initTimer
                mainTimer.Interval = 600000;
                mainTimer.Elapsed += InPlayCheck;
                mainTimer.Enabled = true;
                mainTimer.Stop();

                matchWatchdogTimer.Interval = 10800000;
                matchWatchdogTimer.Elapsed += MachWatchdogTimer_Elapsed;
                matchWatchdogTimer.Enabled = true;
                #endregion
                InPlayCheck(null, null);
            }
            catch (Exception e1)
            {
                //Trace.WriteLine(e1);
                Console.WriteLine(e1);
            }
        }
        public bool Probe()
        {
            bool result = false;
            try
            {
                string resultJson = _client.DownloadString(address);
                var jobj = JsonConvert.DeserializeObject(resultJson);
                var status = JsonValue("fixture.status", jobj);         
                int.TryParse(JsonValue("fixture.result.goalsAwayTeam", jobj), out team2goals);
                int.TryParse(JsonValue("fixture.result.goalsHomeTeam", jobj), out team1goals);
                switch (status)
                {
                    case "SCHEDULED":
                        GoalArrived?.Invoke(this, new DataGoalEventArgs() { State = 0});
                        break;
                    case "TIMED":
                        GoalArrived?.Invoke(this, new DataGoalEventArgs() { State = 0 });
                        break;
                    case "IN_PLAY":
                        if (isIn_Play == false)
                        {
                            GoalArrived?.Invoke(this, new DataGoalEventArgs() { State = 1, Message = "A meccs elkezdődött" });
                            mainTimer.Interval = 60000;
                        }
                        else
                        {
                            if (team1goals > GoalTeamHome)
                            {
                                GoalArrived?.Invoke(this, new DataGoalEventArgs() { goalsAwayTeam = team2goals, goalsHomeTeam = team1goals, State = 1, Team1 = JsonValue("fixture.homeTeamName", jobj), Team2 = JsonValue("fixture.awayTeamName", jobj), Message = $"Gólt lőtt a(z) {JsonValue("fixture.homeTeamName", jobj)} csapat" });
                                GoalTeamHome = team1goals;
                            }
                            if (team2goals > GoalTeamAway)
                            {
                                GoalArrived?.Invoke(this, new DataGoalEventArgs() { goalsAwayTeam = team2goals, goalsHomeTeam = team1goals, State = 1, Team1 = JsonValue("fixture.homeTeamName", jobj), Team2 = JsonValue("fixture.awayTeamName", jobj), Message = $"Gólt lőtt a(z) {JsonValue("fixture.awayTeamName", jobj)} csapat" });
                                GoalTeamAway = team2goals;
                            }
                        }
                        break;
                    case "FINISHED":                        
                        GoalArrived?.Invoke(this, new DataGoalEventArgs() { goalsAwayTeam = team2goals, goalsHomeTeam = team1goals, State = 2, Team1 = JsonValue("fixture.homeTeamName", jobj), Team2 = JsonValue("fixture.awayTeamName", jobj), Message = "A meccs véget ért" });
                        matchWatchdogTimer.Stop();
                        result = true;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + (ex.InnerException != null ? ex.InnerException.Message : ex.Message));
            }
            //var ccccc=  ((Newtonsoft.Json.Linq.JObject)jobj).Descendants().OfType<JObject>().FirstOrDefault(x=>(x.GetValue("status") != null));
            return result;
        }
        private string JsonValue(string prop, object obj)
        {
            var result = string.Empty;
            var array = prop.Split('.');            
            for (int i = 0; i < array.Length - 1; i++)
            {
                if (((Newtonsoft.Json.Linq.JObject)obj).GetValue(array[i]) != null)
                {
                    obj = ((Newtonsoft.Json.Linq.JObject)obj).GetValue(array[i]);
                }
            }
            if (((Newtonsoft.Json.Linq.JObject)obj).GetValue(array[array.Length - 1]) != null)
            {
                result = ((Newtonsoft.Json.Linq.JObject)obj).GetValue(array[array.Length - 1]).ToString();
            }
            return result;
        }
        private void DisposeClass()
        {
            mainTimer.Dispose();
            matchWatchdogTimer.Dispose();
            _client.Dispose();          
        }
        #endregion
    }
}

